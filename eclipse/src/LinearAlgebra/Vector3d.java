//Jimmy Le 1936415
package LinearAlgebra;

public class Vector3d {

	private double x;
	private double y;
	private double z;
	
	
	public Vector3d(double newx, double newy, double newz) {
		x = newx;
		y = newy;
		z = newz;
		
	}
	
	//getters
	public double getx() {	
		return x;
	}
	
	public double gety() {	
		return y;
	}
	
	public double getz() {	
		return z;
	}
	
	//methods
	public double magnitude() {
		double result = 0;
		result = Math.sqrt(( x*x + y*y + z*z ));
		return result;
	}
	
	public double dotProduct(Vector3d second) {
		double result = 0;
		result = (x*second.getx()) + (y*second.gety()) + (z*second.getz());
		return result;
	}
	
	public Vector3d add(Vector3d second) {
		double newx = x + second.getx();
		double newy = y + second.gety();
		double newz = z + second.getz();
		
		Vector3d newV = new Vector3d(newx, newy, newz);
		return newV;
	}
}
