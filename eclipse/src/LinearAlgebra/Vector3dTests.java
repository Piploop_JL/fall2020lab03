//Jimmy Le 1936415
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

//tests on getters
	@Test
	void test1() {
		Vector3d vObject = new Vector3d(1,2,3);
		assertEquals(1, vObject.getx());
	}
	
	@Test
	void test2() {
		Vector3d vObject = new Vector3d(1,2,3);
		assertEquals(2, vObject.gety());
	}
	
	@Test
	void test3() {
		Vector3d vObject = new Vector3d(1,2,3);
		assertEquals(3, vObject.getz());
	}

	
//tests on magnitude()
	
	@Test
	void test4() {
		Vector3d vObject = new Vector3d(1,2,3);
		assertEquals(3.74, vObject.magnitude(), 3.75);
	}
	
	@Test
	void test5() {
		Vector3d vObject = new Vector3d(1,2,2);
		assertEquals(3, vObject.magnitude());
	}
	
	@Test
	void test6() {
		Vector3d vObject = new Vector3d(-1,2,2);
		assertEquals(3, vObject.magnitude());
	}
	
//tests on dotProduct()
	
	@Test
	void test7() {
		Vector3d vObject = new Vector3d(1,1,2);
		Vector3d vObject2 = new Vector3d(2,3,4);
		assertEquals(13, vObject.dotProduct(vObject2));
	}
	
	@Test
	void test8() {
		Vector3d vObject = new Vector3d(-1,1,2);
		Vector3d vObject2 = new Vector3d(2,3,4);
		assertEquals(9, vObject.dotProduct(vObject2));
	}

//tests on add()
	
	
	@Test
	void test9() {
		Vector3d vObject = new Vector3d(1,1,2);
		Vector3d vObject2 = new Vector3d(2,3,4);
		Vector3d vObject3 = vObject.add(vObject2);
		assertEquals(3, vObject3.getx());
	}
	
	@Test
	void test10() {
		Vector3d vObject = new Vector3d(1,1,2);
		Vector3d vObject2 = new Vector3d(2,3,4);
		Vector3d vObject3 = vObject.add(vObject2);
		assertEquals(4, vObject3.gety());
	}
	
	@Test
	void test11() {
		Vector3d vObject = new Vector3d(1,1,2);
		Vector3d vObject2 = new Vector3d(2,3,4);
		Vector3d vObject3 = vObject.add(vObject2);
		assertEquals(6, vObject3.getz());
	}
	
	
}













